package com.example.ljenster.myapplication;

/**
 * Created by ljenster on 29-9-2017.
 */
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LapTime {
    private Date lapTime;
    private Track track;
    private Driver driver;

    public LapTime(String time, Track track, Driver driver){
        this.track = track;
        this.driver = driver;


        DateFormat format = new SimpleDateFormat("h:m:s:S");
        try {
            this.lapTime = format.parse(time);
        } catch (ParseException e) {
            //String errorString = "There was an error with converting the laptime to a date format";
            e.printStackTrace();
        }

    }


    public Date getLapTime() {
        return lapTime;
    }

    public void setLapTime(Date lapTime) {
        this.lapTime = lapTime;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
