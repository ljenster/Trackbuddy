package com.example.ljenster.myapplication;

import android.os.SystemClock;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by ljenster on 29-9-2017.
 */

public class Logic {

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    int Seconds, Minutes, MilliSeconds = 0;

    public Logic(){

    }

    private Track nearestTrack = null;
    boolean firstTime = true;

    public String startLapTimer(double longitude, double latitude){

        if(nearestTrack != null) {

            double trackDistance = calculateDistanceToLocation(longitude, latitude, nearestTrack.getLongtitude(), nearestTrack.getLatitude());

            if (trackDistance < 5) {
                if(firstTime){
                    MillisecondTime = 0L ;
                    StartTime = 0L ;
                    TimeBuff = 0L ;
                    UpdateTime = 0L ;
                    Seconds = 0 ;
                    Minutes = 0 ;
                    MilliSeconds = 0 ;
                    StartTime = SystemClock.uptimeMillis();
                }


                UpdateTime = TimeBuff+StartTime;
                Seconds = (int)(UpdateTime/1000);
                Minutes = Seconds/60;
                Seconds%=60;
                MilliSeconds = (int)(UpdateTime%1000);




            }
        }
        return ""+Minutes+":"+String.format("%2d",Seconds)+":"+String.format("%3d",MilliSeconds);

    }

    public Track findNearestTrack(double longitude, double latitude, ArrayList<Track> trackList){

        if(trackList.size() > 0){
            for (Track track:trackList)
            {

                double trackDistance = calculateDistanceToLocation(track.getLatitude(), track.getLongtitude(), longitude, latitude);
                if(trackDistance < 20.00){
                    nearestTrack = track;
                }

            }
        }
        return nearestTrack;
    }

    public double calculateDistanceToLocation(double trackLatitude, double trackLongitude, double currentLongitude, double currentLatitude){

        double theta = trackLatitude - currentLongitude;
        double dist = Math.sin(deg2rad(trackLongitude))
                * Math.sin(deg2rad(currentLatitude))
                + Math.cos(deg2rad(trackLongitude))
                * Math.cos(deg2rad(currentLatitude))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        double km = dist / 0.62137;
        double meters = Math.round(km / 1000);

        return meters;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
