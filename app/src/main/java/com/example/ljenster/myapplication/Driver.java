package com.example.ljenster.myapplication;

/**
 * Created by ljenster on 29-9-2017.
 */

public class Driver {

    private String name;

    private int age;

    private int level;

    public Driver(String name, int age, int level){
        this.name = name;
        this.age = age;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
