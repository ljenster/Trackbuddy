package com.example.ljenster.myapplication;

/**
 * Created by ljenster on 29-9-2017.
 */

public class Track {
    private double longtitude;
    private double latitude;
    private String trackName;


    public Track(double longtitude, double latitude, String trackName) {
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.trackName = trackName;
    }




    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }
}
