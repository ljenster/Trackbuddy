package com.example.ljenster.myapplication;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private TextView editText;
    private TextView timerValue;
    private ImageButton startLT;

    //database references
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("message");
    DatabaseReference bestLapRef = database.getReference("bestLap");



    private ArrayList<Track> trackList;
    Logic logic;

    private Track nearestTrack;

    Handler customHandler = new Handler();



    private void fillTrackList(){
        DatabaseReference tracks = mRootRef.child("Tracks");
        


        Track track = new Track(5.0141005,51.5838659, "sjaakie");
        trackList.add(track);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }

    };


    // Define a listener that responds to location updates
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            makeUseOfNewLocation(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    private void makeUseOfNewLocation(Location location) {

        nearestTrack =  logic.findNearestTrack(location.getLongitude(), location.getLatitude(), trackList);


        editText.setText(nearestTrack.getTrackName());

    }

// Register the listener with the Location Manager to receive location updates

    Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            String lapTimeUpdate;
            lapTimeUpdate = logic.startLapTimer(nearestTrack.getLongtitude(), nearestTrack.getLatitude());
            timerValue.setText(lapTimeUpdate);
            customHandler.postDelayed(this,0);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int ACCES_FINE_LOCATION = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCES_FINE_LOCATION
            );
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        //mTextMessage = (TextView) findViewById(R.id.message);
        editText = (TextView) findViewById(R.id.editText);
        timerValue = (TextView) findViewById(R.id.timerValue);

        startLT = (ImageButton) findViewById(R.id.start_lt);

        startLT.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                logic.startLapTimer(nearestTrack.getLongtitude(), nearestTrack.getLatitude());
                customHandler.postDelayed(updateTimerThread, 0);
            }
        });


        trackList = new ArrayList<Track>();
        fillTrackList();

        if(trackList.get(0) != null){
            editText.setText(Double.toString(trackList.get(0).getLatitude()));
        }

        logic = new Logic();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        myRef.setValue("Hello, World!");





    }

}
